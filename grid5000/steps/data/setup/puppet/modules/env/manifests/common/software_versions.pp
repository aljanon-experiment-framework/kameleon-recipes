# This file defines the software versions in use

class env::common::software_versions {
   $g5k_subnets = '1.4.2'
   $g5k_meta_packages = '0.7.33'
   $tgz_g5k = '2.0.6'
   $g5k_checks = '0.8.24'
   $sudo_g5k = '1.5'
}
