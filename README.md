# Kameleon recipes

This repository contains steps and recipes for the 
[kameleon](http://kameleon.imag.fr/) image builder. This creates a Guix-enabled
image based on Debian 10. It is based on the `debian10-x64-min` Grid5000 image.
To build this image on a Grid5000 node with all dependencies and parameters
managed automatically, please check the
[image-builder repository](https://gitlab.inria.fr/aljanon/image_builder). The 
image has one main recipe file 
([debian10-min-guix.yaml](./debian10-min-guix.yaml)) and two steps:

- [install_custom_kernel](./steps/install_custom_kernel.yaml):
  - Compiles the kernel with support for additional capabilities:
    - resctrl: cgroup-like interface for specific hardware resource
      partitioning, namely cache partitioning or core memory bandwidth
      partitioning
    - /proc/config: provides the current kernel config in a sysfs file
    - uncore perf events: enables the use of uncore HW counters with perf
    - msr modules: makes the usage of msr module (e.g. `modprobe msr`) possible
    - cpuid: allows using the cpuid instruction, useful for collecting data on the
      system before an experiment
    - default cpu governor to performance
  - In addition, the compiled kernel has the following options:
    - disabled module signing: for now, this makes it easier to compile the
      kernel. An ephemeral security key should be used instead. See [Debian
      kernel
      documentation](https://kernel-team.pages.debian.net/kernel-handbook/ch-common-tasks.html#s-common-building)
      for more information.
    - disabled debug information: speeds up compilation and slims the
      kernel
    - disabled system trusted keys: again, ease the compilation process. Should
      be fixed in the future.
  - The kernel config is based on the config of the current running system.
- [install_guix](./steps/install_guix.yaml): Installs GNU Guix using the
  `guix-install.sh` script provided by the Guix team. The installer is
  located in [steps/data/install-guix.sh].
- [additional_software](./steps/additional_software.yaml): Installs python3 and
  rsync on the system. These are installed outside of GNU Guix for ease of use
  with ansible (for instance with EnOSlib).

A Debian 9 recipe is also available.

## Usage

To build the image, simply run
```bash
kameleon build debian10-min-guix
```

For a Debian 9 image:
```bash
kameleon build debian9-min-guix
```

## Dependencies

To build the image, [kameleon](http://kameleon.imag.fr/) and its dependencies
must be installed on your system. Please see the
[installation instructions](http://kameleon.imag.fr/installation.html)
for more information.

## Repository dependencies

The [Grid5000 environment-recipes repository](https://gitlab.inria.fr/grid5000/environments-recipes)
is used as a git-subrepo to provide the base environment recipes to build upon.
The standard kameleon repository mechanism is not used as a way to ensure
reproducibility.

## Customization

[debian10-min-guix.yaml](./debian10-min-guix.yaml) is the main recipe image. It
extends the `debian10-x64-min` Grid5000 image and has various customization
options:

- g5k_kernel_params: Kernel boot parameters. This is configured to
  `rdt=l3cat,!mba` to activate L3 CAT for RDT and deactivate mba.
- [guix-install.sh](./steps/data/guix-install.sh): The standard guix installer
  downloaded from
  [https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh].
  To download from a specific commit, add `?h=<hash>` at the end of the url.

## Future work

### Debian snapshots

A specific Debian snapshot ([https://snapshot.debian.org/]) should be used
instead of the default Debian repositories. This would help ensuring that the
image can be rebuilt in the exact same way in the future.

The `deb_mirror_uri` and `deb_mirror_hostname` kameleon recipe parameters
are potential candidates.

### Fixed kernel

Fixing a kernel version is hard for Debian images as it may change with updates
to the main Debian repository. Fixing a Debian snapshot would probably
remove this problem altogether, allowing to have a specific version of the
kernel.

### Enabling module signing and system trusted keys

Instead of disabling completely these two mechanisms, they should be configured
properly.

### Version-controlled kernel config file

Instead of relying on the current kernel config, we should provide a
version-controlled config file with the options we need. To make it easy to
work, this should be provided only with a fixed kernel version (as the options
may change between versions).
